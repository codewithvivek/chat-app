const mongoose = require('mongoose')

const { Schema } = mongoose

const ChatSchema = new Schema({
    name:{
        type: String,
        required: true
    },
    message:{
        type: String,
        required: true
    },
    timestamp: {
        type: String,
        required: true
    },
    sender: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    groupid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Group",
        required: true
    }
})

const Chat = mongoose.model('Chat', ChatSchema)
module.exports=Chat;