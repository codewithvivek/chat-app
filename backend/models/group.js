const mongoose = require('mongoose')

const { Schema } = mongoose

const GroupSchema = new Schema({
    name:{
        type: String,
        required: true
    },
    members:{
        type:Array,
        required: true,
        ref: 'User'
    },
    admins:{
        type:Array,
        required: true
    },
    timestamp: {
        type: String,
        required: true
    }
})

const Group = mongoose.model('Group', GroupSchema)
module.exports=Group;