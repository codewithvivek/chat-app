const mongoose = require('mongoose')

const User = require('./user')
const Group = require('./group')
const Chat = require('./chats')

let connectOpts = {}

connectOpts = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  autoCreate: true,
  retryWrites: false,
  autoIndex: true,
}

mongoose.connect(process.env.MONGO_HOST, connectOpts).then(async (con) => {
  console.log('Mongoose connected to Mongo')
}).then(async (con) => {
  // initializing models because if used first time within transactions, it will error with
  // MongoServerError: Cannot run 'listCollections' in a multi-document transaction.
  console.log('---- Initializing Models ----')
  console.log(`Users:\t\t${await User.count()}`)
  console.log(`Groups:\t\t${await Group.count()}`)
  console.log(`Chats:\t\t${await Chat.count()}`)
  console.log('---- Model Init Complete----')
})
  .catch((err) => {
    console.log('Connection to Mongo Failed')
    console.error(err)
  })

module.exports.mongoose = mongoose
module.exports.User = User
module.exports.Group = Group
module.exports.Chat = Chat