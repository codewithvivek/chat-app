const { models, model } = require('mongoose');

exports.getChats = async (req, res, next)=>{
    await models.Chat.find({groupId:parseInt(req.query.groupId)}).lean().then((chat)=>{
        return res.status(200).send(chat)
    }).catch(err=>console.log(err))
}

exports.postChat = async (req, res, next) => {
    console.log(req.user.id)
    const [chat] = await models.Chat.create([{
        name:req.user.name,
        message:req.body.message,
        userId:req.user._id,
        groupId:req.body.groupId
    }])
    await chat.save().then(response=>{
        res.status(201).send(response)
    }).catch(err=>console.log(err))
}