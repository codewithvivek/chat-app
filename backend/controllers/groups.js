const { models } = require('mongoose');

exports.getUsers = async (req, res, next)=>{
    const users = await models.User.find({}).lean()
    let data=[]
    if (users.length>0){
        users.map(user=>{
            if(!req.user._id.equals(user._id)){
                let entry={id:user._id, name:user.name}
                data.push(entry)
            }
        })
    }
    res.status(200).send(data)
}

exports.findGroups = async (req, res, next) => {
    await models.Group.find().lean().then((groups)=>{
        let usergroups=[]
        groups.map(group=>{
            let members=group.members
            if(members.includes(req.user.id)){
                usergroups.push(group)
            }
        })
        res.status(200).send(usergroups)
    })
}

exports.createGroup = async (req, res, next)=>{
    const [group] = await models.Group.create([{
        name:req.body.name,
        members:[...req.body.members, req.user.id],
        admins:[req.user._id],
        timestamp: new Date().toLocaleDateString()
    }])
    await group.save().then(response=>{
        res.status(201).send({code: 1, message: 'Group created successfully!', result: response})
    }).catch(err=>console.log(err))
}

exports.getGroupDetails = async (req, res, next) => {
    await models.Group.findOne({_id: req.query.groupId}).populate('members').then((group)=>{
        return res.status(200).send({group:group, isAdmin:group.admins.includes(req.user._id), me:req.user}) 
    }).catch((err)=> {
        return res.status(200).send({code:0, message: 'Group does not exist!'})
    })
}

exports.addAdmin = async (req, res, next)=>{
    const group = await models.Group.findById(req.body.id)
    group.admins.push(req.body.admin)

    await group.save().then((response) => {
        res.status(201).send(response)
    }).catch(err=>console.log(err))
}

exports.removeAdmin = async (req, res, next) => {
    const group = await models.Group.findById(req.body.id)
        group.admins.splice(group.admins.indexOf(req.body.admin),1)
    
    await group.save().then(response=>{
        res.status(201).send(response)
    }).catch(err=>console.log(err))
}

exports.removeUser = async (req, res, next) => {
    let removeGroup = false
    const group = await models.Group.findById(req.body.id)
    group.members.splice(group.members.indexOf(req.body.member || req.body.token),1)
    let index=group.admins.indexOf(req.body.member || req.body.token)
    if (index>-1){
        group.admins.splice(index, 1)
    }
    if (group.members.length == 0){
        removeGroup = true
    }
    
    await group.save().then(async(response)=>{
        if(removeGroup){
            await models.Group.findByIdAndDelete(req.body.id)
        }
        if(response){
            res.status(201).send({response: response, success:true})
        }
    }).catch(err=>console.log(err))
}

exports.addUsers = async (req, res, next) => {
    await models.Group.findById(req.body.id).then(group=>{
        group.members=[...group.members, ...req.body.users]
    }).catch(err=>console.log(err))
    await group.save().then(response=>{
        res.status(201).send(response)
    }).catch(err=>console.log(err))
}