var jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const { models } = require('mongoose');
const saltRounds = 10;

function generateToken(email){
    return (jwt.sign({email:email}, 'chatAppToken'))
}

exports.createUser = async (req, res, next)=>{
    console.log(req.body)
    let user = await models.User.findOne({email:req.body.email}).lean()
    if (!user){
        bcrypt.hash(req.body.password, saltRounds).then(async (hash)=>{
            const [user] = await models.User.create([{
                name: req.body.name,
                phone:req.body.phone,
                email: req.body.email,
                password: hash,
            }])
            await user.save().then(response=>{
                res.status(201).send({code: 1, message: 'User sign up successful!'})
            }).catch(err=>console.log(err))
        })
    } else {
        res.status(201).send({code: 2, message: 'User already exists!'})
    }
}

exports.findUser = async (req, res, next)=>{
    const creds=JSON.parse(req.params.creds)
    let user = await models.User.findOne({email:creds.email}).select('password').lean()
    if (!user){
        res.status(200).send({code:0})
    }else{
        bcrypt.compare(creds.password, user.password).then((result)=>{
            if(result){
                res.status(200).send({code:1, token:generateToken(creds.email), userId:user._id})
            }else{
                res.status(200).send({code:2})
            }
        });
    }
}

