var jwt = require('jsonwebtoken');
const { models } = require('mongoose');

exports.authenticate = async (req, res, next)=>{
    try{
        const token=req.header('Authorization')
        const decoded=jwt.verify(token, 'chatAppToken')
        let user = await models.User.findOne({email:decoded.email})
        if (user){
            req.user=user;
            next()
        }else{
            return res.status(401)
        }
    }
    catch (err){
        console.log(err)
    }
}