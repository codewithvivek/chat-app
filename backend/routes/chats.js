const express = require('express');

const chatsController = require('../controllers/chats');
const authenticator=require('../controllers/auth')

const router = express.Router();

router.get('/show', authenticator.authenticate, chatsController.getChats)

router.post('/send',authenticator.authenticate, chatsController.postChat)

module.exports=router;