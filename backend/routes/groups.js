const express = require('express');

const groupsController = require('../controllers/groups');

const authenticator=require('../controllers/auth')

const router = express.Router();

router.get('/allusers', authenticator.authenticate, groupsController.getUsers)

router.get('/find', authenticator.authenticate, groupsController.findGroups)

router.get('/details', authenticator.authenticate, groupsController.getGroupDetails)

router.put('/addAdmin', authenticator.authenticate, groupsController.addAdmin)

router.put('/removeAdmin', authenticator.authenticate, groupsController.removeAdmin)

router.put('/removeUser', authenticator.authenticate, groupsController.removeUser)

router.put('/addUsers', authenticator.authenticate, groupsController.addUsers)

router.post('/create', authenticator.authenticate, groupsController.createGroup)

module.exports=router;