const express = require('express');

const usersController = require('../controllers/user');

const authenticator=require('../controllers/auth')
// const mailer=require('../controllers/mailer')

const router = express.Router();

//Sign In User
router.get('/signin/:creds', usersController.findUser)

//Sign Up User
router.post('/signup', usersController.createUser)

module.exports=router;