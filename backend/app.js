const express = require('express')
const bodyParser = require('body-parser')
const cors=require('cors')
const helmet = require('helmet')

const app=express();

app.use(cors())
app.use(helmet())

app.use(bodyParser.json({extended:false}))
app.use(bodyParser.urlencoded({extended:false}))

const indexRouter = require('./routes/index')
const authRouter = require('./routes/user')
const chatRouter = require('./routes/chats')
const groupRouter = require('./routes/groups')

app.use('/', indexRouter)
app.use('/auth', authRouter)
app.use('/chat', chatRouter)
app.use('/group', groupRouter)

module.exports = app
