import { Component, OnInit } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { AuthService } from 'src/swagger';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  signinBody = {
    email: "",
    password: ""
  }
  constructor(private authService:AuthService, private router: Router){}

  ngOnInit(): void {
      if (localStorage.getItem('token')){
        this.router.navigateByUrl('/chat/groups')
      }
  }

  signin(){
    if(this.signinBody.password.length<=3){
      alert("Enter a strong password!")
      return
    }
    this.authService.signin(JSON.stringify(this.signinBody)).subscribe((response)=>{
      if(response.code == 1){
        alert('Sign In Successful!')
        localStorage.setItem('token', response.token)
        this.router.navigateByUrl('/chat/groups')
        //Redirect user to Chat app
      }
      else if(response.code == 2){
        alert("Invalid credentials!")
      }
      else if(response.code == 0){
        alert("You are not registered with us! Please sign up...")
        this.router.navigateByUrl('/auth/signup')
      }
    })
  }
}
