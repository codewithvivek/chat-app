import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/swagger';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  signupBody = {
    name: "",
    email: "",
    phone: "",
    password: ""
  }
  constructor(private authService:AuthService, private router: Router) {}

  ngOnInit(): void {
    if (localStorage.getItem('token')){
      this.router.navigateByUrl('/chat/groups')
    }
}

  signup(){
    console.log(this.signupBody)
    //Todo -> Subscribe to the signup api
    this.authService.signup(this.signupBody).subscribe((response)=> {
      console.log(response)
      if (response.code == 2){
        alert("You are already registered with us! Please sign in...")
        this.router.navigateByUrl('/auth/signin')
      }
      else if(response.code == 1){
        alert("Sign up successful!")
        this.router.navigateByUrl('/auth/signin')
      }
    })
  }
}
