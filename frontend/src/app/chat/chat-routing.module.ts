import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GroupsComponent } from './groups/groups.component';
import { ManageComponent } from './manage/manage.component';
import { UiComponent } from './ui/ui.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'groups',
    pathMatch: 'full',
  },
  {
    path: 'groups',
    component: GroupsComponent,
  },
  {
    path: 'manage',
    component: ManageComponent,
  },
  {
    path: 'ui',
    component: UiComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChatRoutingModule {}
