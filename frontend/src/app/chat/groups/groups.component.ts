import { Router } from '@angular/router';
import { GroupsService } from './../../../swagger/api/groups.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {
  showNoGroupsCreated = false
  showAddMembersPopup = false
  showCreateGroup = true
  groups: any = []
  allUsers: any = []
  checkedUsers: any = []
  showNextButton: boolean = false
  showPopup: boolean = false
  newGroupName: string = ""


  constructor(private groupService: GroupsService, private router:Router){}

  ngOnInit(){
    let token = localStorage.getItem('token')
    if(!token){
      this.router.navigateByUrl('')
    }
    else{
      this.groupService.findGroups().subscribe(response=>{
        if(response.length == 0) {
          this.showNoGroupsCreated = true
        }
        else {
          this.groups = response
        }
      })
    }
  }

  showChats(id:any){
    sessionStorage.setItem('groupId', id)
    this.router.navigateByUrl('/chat/ui')
  }

  logout(){
    localStorage.removeItem('token')
    this.router.navigateByUrl('')
  }

  showAddMembersPopupHandler(){
    this.showCreateGroup=false
    this.showAddMembersPopup=true
    this.groupService.getusers().subscribe(response=>{
      this.allUsers=response
    })
  }

  selectUser(id:any){
    if(this.checkedUsers.includes(id)){
      this.checkedUsers.splice(this.checkedUsers.indexOf(id), 1)
    }else{
      this.checkedUsers.push(id)
    }
    this.checkedUsers.length>0?this.showNextButton=true:this.showNextButton=false;
  }

  checkUserSelected(id:any){
    if(this.checkedUsers.includes(id)){
      return true
    }else{
      return false
    }
  }

  setGroupName(){
    this.showPopup=true
    this.showAddMembersPopup = false
    this.showNextButton=false
  }

  createGroup(){
    this.groupService.createGroup({name: this.newGroupName, members:this.checkedUsers}).subscribe((response)=>{
      if(response.code == 1){
        location.reload()
      }
    })
  }

  goBack(){
    if(this.showCreateGroup==false){
      this.showCreateGroup=true
      this.showAddMembersPopup=false
    }
  }
}
