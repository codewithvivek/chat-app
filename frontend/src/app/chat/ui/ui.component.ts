import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ChatsService, GroupsService } from 'src/swagger';

@Component({
  selector: 'app-ui',
  templateUrl: './ui.component.html',
  styleUrls: ['./ui.component.css']
})
export class UiComponent implements OnInit{
  messages = [];
  newMessage = '';
  groupName : string = "";
  groupId : string = "";
  isSendDisabled: boolean = true

  constructor(private router: Router, private chatService: ChatsService, private groupService: GroupsService) {}

  ngOnInit(){
    let token = localStorage.getItem('token')
    if(!token){
      this.router.navigateByUrl('')
    }
    this.groupId = sessionStorage.getItem('groupId') as string
    if(!this.groupId || this.groupId == ""){
      this.router.navigateByUrl('/chat/groups')
    }
    this.groupService.getGroupDetails(this.groupId as string).subscribe((response)=> {
      console.log(response)
      this.groupName = response.group.name
    })
    this.chatService.getChats(this.groupId as string).subscribe((response)=> {
      console.log(response)
    })
  }

  showGroupInfo(){
    this.router.navigateByUrl('/chat/manage')
  }

  goBack(){
    sessionStorage.removeItem('groupId')
    this.router.navigateByUrl('/chat/groups')
  }

  checkInput(){
    console.log(this.newMessage)
    this.newMessage.length>0?this.isSendDisabled=false:this.isSendDisabled=true
  }
}
