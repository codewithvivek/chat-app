import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { GroupsService } from 'src/swagger';

@Component({
  selector: 'app-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.css']
})
export class ManageComponent implements OnInit{
  groupName: string = ""
  groupId: string = ""
  members: any = []
  admins: any = []
  token: string = ""
  memberisAdmin: boolean = false
  me: any = {}
  showUsersPopup: any = []
  showAddParticipants: boolean = false
  canAddMembers: any = []
  constructor(private groupsService: GroupsService, private router:Router) {}

  ngOnInit(): void {
    this.token = localStorage.getItem('token') as string
    if(!this.token || this.token == ""){
      this.router.navigateByUrl('')
      return
    }
    this.groupId = sessionStorage.getItem('groupId') as string
    if(!this.groupId || this.groupId == ""){
      this.router.navigateByUrl('/chat/groups')
      return
    }
    this.groupsService.getGroupDetails(this.groupId).subscribe(response=>{
      this.groupName=response.group.name
      this.members=[...response.group.members]
      this.admins = response.group.admins
      this.memberisAdmin = response.isAdmin
      this.me = response.me
      this.members.forEach((entry:any, ind: number) => {
        this.members[ind]['showPopup'] = false
      });
    })
  }

  showUserPopup(id : any){
    this.members.forEach((entry : any) => {
      if(entry._id == id){
        entry.showPopup = !entry.showPopup
      }
    });
  }

  isAdmin(id:any){
    if(this.admins.includes(id)) {
      return true
    }else{
      return false
    }
  }

  makeAdmin(id:any){
    this.groupsService.addAdmin({id: this.groupId, admin: id}).subscribe(response =>{
      this.ngOnInit()
    })
  }

  dismissAdmin(id:any){
    this.groupsService.removeAdmin({id: this.groupId, admin: id}).subscribe(response =>{
      this.ngOnInit()
    })
  }

  leaveGroup(){
    let data = {
      id : this.groupId,
      token: this.token
    }
    this.groupsService.removeUser(data).subscribe(response => {
      if(response.success){
        sessionStorage.removeItem('groupId')
        this.ngOnInit()
      }
    })
  }

  showAddParticipantsHandler(){
    this.showAddParticipants= !this.showAddParticipants
    this.groupsService.getusers().subscribe(response => {
      console.log(response)
      const existingIds : any = []
      this.members.forEach((entry:any)=>{
        existingIds.push(entry._id)
      })
      
      console.log(existingIds,this.canAddMembers)
    })
  }

}
