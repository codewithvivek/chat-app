export * from './auth.service';
import { AuthService } from './auth.service';
export * from './chats.service';
import { ChatsService } from './chats.service';
export * from './groups.service';
import { GroupsService } from './groups.service';
export const APIS = [AuthService, ChatsService, GroupsService];
